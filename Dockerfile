FROM debian:stable-slim

LABEL maintainer="Be Wilson (@bespike)"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get install -y tar wget build-essential libssl-dev apache2-utils

RUN wget http://www.squid-cache.org/Versions/v5/squid-5.4.1.tar.gz && tar xzf squid-5.4.1.tar.gz

WORKDIR squid-5.4.1

RUN ./configure

RUN make